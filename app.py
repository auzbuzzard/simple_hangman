import time

def main():

    for i in range(1, 10 + 1):
        s = f"Loop #{i:02}"
        print(s, end="\r" if i < 10 else "\n")
        time.sleep(1)


if __name__ == "__main__":
    main()
